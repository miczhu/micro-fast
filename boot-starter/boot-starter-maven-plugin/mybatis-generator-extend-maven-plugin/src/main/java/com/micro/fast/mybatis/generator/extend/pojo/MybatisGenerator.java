package com.micro.fast.mybatis.generator.extend.pojo;

import java.util.Map;

/**
 * mybatisGenerator的配置参数对象
 */
public class MybatisGenerator {
  private String pojoPackage;

  private String daoPackage;

  private String tablesPrefix;

  private Map<String,String> tablesNameAndDoMainObjectsName;

  public Map<String, String> getTablesNameAndDoMainObjectsName() {
    return tablesNameAndDoMainObjectsName;
  }

  public void setTablesNameAndDoMainObjectsName(Map<String, String> tablesNameAndDoMainObjectsName) {
    this.tablesNameAndDoMainObjectsName = tablesNameAndDoMainObjectsName;
  }

  public String getPojoPackage() {
    return pojoPackage;
  }

  public void setPojoPackage(String pojoPackage) {
    this.pojoPackage = pojoPackage;
  }

  public String getDaoPackage() {
    return daoPackage;
  }

  public void setDaoPackage(String daoPackage) {
    this.daoPackage = daoPackage;
  }

  public String getTablesPrefix() {
    return tablesPrefix;
  }

  public void setTablesPrefix(String tablesPrefix) {
    this.tablesPrefix = tablesPrefix;
  }
}
