package com.micro.fast.boot.starter.common.util;

/**
 * 对异常处理的工具类
 * @author lsy
 */
public class ExceptionUtil {
  /**
   * 分隔符
   */
  public static final String split = "::::";

  /**
   * 合并异常代码和描述
   * @param code
   * @param msg
   * @return
   */
  public static String contactCodeMsg(Integer code,String msg){
    return String.valueOf(code)+ExceptionUtil.split+msg;
  }

  /**
   * 拆分异常描述
   * @return
   */
  public static String[] splitCodeMsg(String codeMsg){
    String[] split = codeMsg.split(ExceptionUtil.split);
    return split;
  }
}
