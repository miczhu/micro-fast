package com.micro.fast.boot.starter.common.exception;

/**
 * 用户异常类
 * @author lsy
 */
public class UserException extends RuntimeException{
  public UserException(String message) {
    super(message);
  }
}
