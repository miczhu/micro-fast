package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.exception.UserException;
import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.dao.UpmsUserMapper;
import com.micro.fast.upms.pojo.UpmsUser;
import com.micro.fast.upms.service.UpmsUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 权限管理系统用户管理
 *
 * @author lsy
 */
@RestController
@RequestMapping(value = "/upms/user")
@Api(description = "后台用户api")
public class UpmsUserController {

  /**
   * 用户服务接口
   */
  @Autowired
  private UpmsUserService upmsUserService;

  @Autowired
  private UpmsUserMapper upmsUserMapper;

  /**
   * 用户注册,使用Valid注解对upmsUser进行参数校验
   * BindingResult一定要紧跟校验的对象
   * @return
   */
  @PostMapping
  @ApiOperation(value = "后台用户注册")
  public ServerResponse<UpmsUser> register(@Valid  UpmsUser upmsUser,BindingResult errors
      , @RequestParam(defaultValue = "") String repeatPassword) {
    //对参数的校验抛出异常
    if (errors.hasErrors()) {
      errors.getAllErrors().stream().forEach(objectError -> {
        FieldError fieldError = (FieldError) objectError;
        throw new UserException(fieldError.getDefaultMessage());
      });
    }
    ServerResponse<UpmsUser> register = upmsUserService.register(upmsUser, repeatPassword);
    return register;
  }

  @GetMapping("/{username}")
  @ApiOperation(value = "根据用户名称获取用户详细信息")
  public ServerResponse getUserByName(@PathVariable(name = "username") String username){
      UpmsUser upmsUser = upmsUserMapper.selectByUsername(username);
      if (upmsUser ==null){
        return ServerResponse.errorMsg("该用户不存在!");
      }
      if (upmsUser.getLocked() == 1){
        return ServerResponse.errorMsg("账户被锁定");
      }
      return ServerResponse.successData(upmsUser);
  }
}
